var express = require("express");
var cors = require("cors");
var app = express();
var port = 3001
var ciudades = ["Paris","Barcelona","Barranquilla","Montevideo","Santiago de Chile","Mexico DF","Nueva York"];
app.use(express.json());
app.use(cors());
app.listen(port,() => console.log("Server running on port "+port));
app.get("/ciudades",(request,response,next) => 
	response.json(
		ciudades.filter(
			(ciudad) =>
					 ciudad.toLowerCase()
					 .indexOf(
					 	request.query.q.toString().toLowerCase()
					 	) > -1 
			)
		)
	);

var misDestinos = [];
app.get("/my",(request,response,next) => response.json(misDestinos));
app.post("/my",(request,response,next) => {
	console.log(request.body);
	misDestinos.push(request.body.nuevo);
	response.json(misDestinos);
});
var translates = [{
	lang:"es",
	key:"name",
	value:"nombre"
},{
	lang:"en",
	key:"name",
	value:"name"
},{
	lang:"fr",
	key:"name",
	value:"nombe"
},{
	lang:"es",
	key:"hello",
	value:"Hola"
},{
	lang:"en",
	key:"hello",
	value:"hello"
},{
	lang:"fr",
	key:"hello",
	value:"benvenite"
}];
app.get("/api/translate",(request,response,next) => 
	response.json( translates.filter((translate)=>translate.lang == request.query.lang))
);

var tags = [];
app.get("/api/tag",(request,response,next) => {
	const name = request.query.name;
	const count = request.query.count;
	    var found = tags.filter((tag) => tag.key == name);
	    if(found.length > 0){
	    	found.forEach((tag) => tag.value = count);
	    }else{
			tags.push({
				key:name,
				value:count
			});
		}
		response.json(tags);
	});
app.get("/api/tags",(request,response,next) => {
	response.json(tags);
});