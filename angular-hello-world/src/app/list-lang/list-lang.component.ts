import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-lang',
  templateUrl: './list-lang.component.html',
  styleUrls: ['./list-lang.component.scss']
})
export class ListLangComponent implements OnInit {
	listLangs : string[]

  constructor() { 
  	this.listLangs = ['Angular','Android','Arduino','Java','Gosu','JavaScript'];
  }

  ngOnInit(): void {
  }

}
