import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaludadorComponent } from './saludador/saludador.component';
import { ListLangComponent } from './list-lang/list-lang.component';
import { LangComponent } from './lang/lang.component';

@NgModule({
  declarations: [
    AppComponent,
    SaludadorComponent,
    ListLangComponent,
    LangComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
