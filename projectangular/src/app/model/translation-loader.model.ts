import { Injectable } from '@angular/core';
import { DataBase as database } from './databaseconnect.model';
import { Translation } from './translation.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader } from '@ngx-translate/core';
import { APP_CONFIG_API } from './api-config.model';
import { TranslateService } from '@ngx-translate/core';

export class TranslationLoader implements TranslateLoader{

	constructor( private http: HttpClient ){
		console.log("creando translationLoader")
	}

	getTranslation(lang: string): Observable<any>{
		console.log("Obteniendo traducciuones");
		const promise = database.translation.where("lang").equals(lang).toArray()
						.then( results => {
							if(results.length === 0){
								console.log("No se encontro almacenamiento local");
								var requestTo = APP_CONFIG_API.apiEndPoint+APP_CONFIG_API.translationByLang+lang;
								console.log("Request To::"+requestTo);
								return this.http.get<Translation[]>(requestTo)
										.toPromise().then(response => {
											console.log("respuesta::",response);
											database.translation.bulkAdd(response);
											return response;
										});
							}
							console.log("registros encontrados en BD",results);
							return results;
						}).then((translations) => {
									var map = translations.map((translation) => ({[translation.key]: translation.value}));
									console.log("Keypair:.",map)
									return map;
								}
								);
		console.log("finalizacion la Obtencion de las traducciuones");						
		return from(promise).pipe(flatMap((elemens) => from( elemens)));
	}
}

export function  httpLoaderFactory(http: HttpClient){
	return new TranslationLoader(http);
}