import { initializeObjectoState, LikeAction, InitAction, reducerObjecto, NuevoAction, ULikeAction } from './objecto-state.model';
import { Objecto } from './objecto.model';

describe("reducer objectos",()=>{
	it("reducer should initaction",()=>{
		const obj = new Objecto("name","describe");
		const state =  initializeObjectoState();
		const action = new InitAction([obj]);

		const result = reducerObjecto(state,action);

		expect(result.items.length).toEqual(1);
		expect(result.items[0].nombre).toEqual(obj.nombre)
		expect(result.items[0].descripcion).toEqual(obj.descripcion)
	});
	
	it("reducer should nuevo action",()=>{
		const obj = new Objecto("name","descripcion");
		const state = initializeObjectoState();
		const action = new NuevoAction(obj);

		const result = reducerObjecto(state,action);

		expect(result.items.length).toEqual(1);
		expect(result.items[0].nombre).toEqual(obj.nombre);
		expect(result.items[0].descripcion).toEqual(obj.descripcion);
	});

	it("reducer should like action",()=>{
		const obj = new Objecto("name","descripcion");
		const state = initializeObjectoState();
		const action = new LikeAction(obj);
		state.items = [obj];

		const result = reducerObjecto(state,action);

		expect(result.items.length).toEqual(1);
		expect(result.items[0].nombre).toEqual(obj.nombre);
		expect(result.items[0].descripcion).toEqual(obj.descripcion);
	});

	it("reducer should unlike action",()=>{
		const obj = new Objecto("name","descripcion");
		const state = initializeObjectoState();
		const action = new ULikeAction(obj);
		state.items = [obj];

		const result = reducerObjecto(state,action);

		expect(result.items.length).toEqual(1);
		expect(result.items[0].nombre).toEqual(obj.nombre);
		expect(result.items[0].descripcion).toEqual(obj.descripcion);
	});
});