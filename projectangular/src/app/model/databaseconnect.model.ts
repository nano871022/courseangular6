import Dexie from 'dexie';
import { Translation } from './translation.model';
import { Injectable } from '@angular/core';
import { Objecto } from './objecto.model';

@Injectable({providedIn: 'root'})
class DataBaseConnect extends Dexie{
	translation: Dexie.Table<Translation, number>;
	forms: Dexie.Table<Objecto, number>;
	constructor(){
		super("DataBaseConnect");
		console.log("Creando conexion a la BD");
		this.version(1).stores({
			translation: '++id, lang, key, value'
		});
		this.version(2).stores({
			translation: '++id, lang, key, value',
			forms: '++id, nombre, descripcion, like, ulike'
		});
		console.log("Fin del constructo de la conexion a la BD");
	}	
}

export const DataBase = new DataBaseConnect();