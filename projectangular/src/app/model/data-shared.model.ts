import { Objecto } from './objecto.model'
export class DataShared{
	data:Objecto[]
	constructor(){
		this.data = []; 
	}
	add(obj:Objecto){
		const size = this.data.length;
		this.data[size] = obj;
	}

	getAll(){
		return this.data;
	}
}