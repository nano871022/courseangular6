export class Objecto{
	like:number;
	ulike:number;
	constructor(public nombre:string,public descripcion:string){
		this.like = 0;
		this.ulike = 0;
	}

	addLike(){
		this.like = this.like + 1;
	}

	getLike():number{
		return this.like;
	}

	addULike(){
		this.ulike = this.ulike + 1;
	}

	getULike():number{
		return this.ulike;
	}

}
export class ObjectDTO{
	public name:string;
	public description:string;
	public like:number;
	public unlike:number;
}