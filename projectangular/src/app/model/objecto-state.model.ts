import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Objecto } from './objecto.model';
import { Tag } from './tag.model';

export interface ObjectoState{
	items:Objecto[];
	loading:boolean;
}

export const initializeObjectoState = function(){
	return{
		items: [],
		loading:false
	}
}

export enum ObjectoActionType{
	NUEVO_OBJECTO = "[OBJECTO] Nuevo",
	VOTE_LIKE = "[OBJECTO] Like",
	VOTE_ULIKE = "[OBJECTO] ULike"
	,INIT = "[OBJECTO] Init"
}

export class InitAction implements Action{
	type = ObjectoActionType.INIT;
	constructor(public objs:Objecto[]){}
}

export class NuevoAction implements Action{
	type = ObjectoActionType.NUEVO_OBJECTO
	constructor(public obj:Objecto){}
}

export class LikeAction implements Action{
	type = ObjectoActionType.VOTE_LIKE
	constructor(public obj:Objecto){}
}

export class ULikeAction implements Action{
	type = ObjectoActionType.VOTE_ULIKE
	constructor(public obj:Objecto){}
}

export type ObjectoAction = NuevoAction | LikeAction | ULikeAction | InitAction;

export function reducerObjecto(state:ObjectoState,action:ObjectoAction){
	switch(action.type){
		case ObjectoActionType.NUEVO_OBJECTO:
			return {...state
					,items:[...state.items,(action as NuevoAction).obj]
				}
		case ObjectoActionType.VOTE_LIKE:
			const objLike : Objecto = (action as LikeAction).obj;
			objLike.addLike();
			return {...state}
		case ObjectoActionType.VOTE_ULIKE:
			const objULike : Objecto = (action as ULikeAction).obj;
			objULike.addULike();
			return {...state}
		case ObjectoActionType.INIT:
			const objs : Objecto[] = (action as InitAction).objs;
			return {... state,
					items: objs
					};
	}
	return state;
}

@Injectable({providedIn: 'root'})
export class ObjectoEffects{
	@Effect()
	nuevoAgregado: Observable<Action> = this.actions.pipe(
		ofType(ObjectoActionType.NUEVO_OBJECTO),
		map((action:NuevoAction)=>new LikeAction(action.obj))
	);
	constructor(private actions:Actions){}
}

export interface TrackState{
	items: Tag[],
	loading: boolean
}

export const initializeTrackState = function(){
	return{
		items: [],
		loading:false
	}
}

export enum TagsType{
	NEW_TAG = "[TAG] Nuevo"
}

export class Tags implements Action{
	type = TagsType.NEW_TAG;
	constructor(public tag: Tag){console.log("Agregando tag ");}
}
export type TagsAction = Tags;

export function reducerTags(state:TrackState,action:TagsAction){
	switch(action.type){
		case TagsType.NEW_TAG:
			const find = state.items.filter((item)=> item.tag == (action as Tags).tag.tag);
			if(find.length > 0){
				find.forEach((item) => item.count = item.count+1);
			}else{
				const tag = (action as Tags).tag;
				tag.count = 1;
				return {...state
					,items:[...state.items,tag]
				}
			}
	}
	return state;
}