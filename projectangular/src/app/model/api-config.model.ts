import { InjectionToken } from '@angular/core';

export interface AppConfig{
	apiEndPoint: string;
	translationByLang : string;
	addForm: string;
	addLike: string;
	addUnLike: string;
	getAll: string;
}

export const APP_CONFIG_API: AppConfig = {
	apiEndPoint: "http://localhost:3001",
	translationByLang: "/api/translate?lang="
	,addForm: "/api/form?"
	,addLike: "/api/like?name="
	,addUnLike: "/api/unlike?name="
	,getAll: "/api/forms"
};

export const APP_CONFIG = new InjectionToken<AppConfig>("app.config");