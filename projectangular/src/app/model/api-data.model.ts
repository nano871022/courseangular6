import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AppState } from '../app.module';
import { Store } from '@ngrx/store';
import { Objecto, ObjectDTO } from './objecto.model';
import { NuevoAction, LikeAction, ULikeAction, InitAction } from './objecto-state.model';
import { APP_CONFIG_API } from './api-config.model';
import { DataBase as database } from './databaseconnect.model';

@Injectable()
export class ApiStore{

	constructor(private store:Store<AppState>,private http: HttpClient){}

	add(obj: Objecto){
    var url = APP_CONFIG_API.apiEndPoint+APP_CONFIG_API.addForm+"name="+obj.nombre+"&description="+obj.descripcion;
    this.http.post<ObjectDTO[]>(url,{}).toPromise();
    /*
    .then(response => response.map((valor)=>{
      var obj = new Objecto(valor.name,valor.description);
      obj.like = valor.like;
      obj.ulike = valor.ulike;
      return obj;
    }));
    */
		this.store.dispatch(new NuevoAction(obj));
    const db = database;
    db.forms.add(obj);
	}

	getAll():Objecto[]{
    	let items : Objecto[];
    	this.store.subscribe((data) => items =data.objectos.items);
    	console.log("Cantidad de registros:",items.length);

    	return items;
  	}	
  	
  	like(obj: Objecto){
      var url = APP_CONFIG_API.apiEndPoint+APP_CONFIG_API.addLike+obj.nombre;
      this.http.post<ObjectDTO[]>(url,{}).toPromise();
    	this.store.dispatch(new LikeAction(obj));
  	}

  	unlike(obj: Objecto){
        var url = APP_CONFIG_API.apiEndPoint+APP_CONFIG_API.addUnLike+obj.nombre;
        this.http.post<ObjectDTO[]>(url,{}).toPromise();
   	    this.store.dispatch(new ULikeAction(obj));
  	}
}
@Injectable()
export class InitializeApiStore{
  constructor(private store:Store<AppState>,private http: HttpClient){}
  async initializeAppState():Promise<any> {
    var url = APP_CONFIG_API.apiEndPoint+APP_CONFIG_API.getAll;
    this.http.get<ObjectDTO[]>(url).toPromise().then((response)=>
      response.map((valor) => {
        var obj = new Objecto(valor.name,valor.description);
        obj.like = valor.like;
        obj.ulike = valor.unlike;
        return obj;
      })).then((forms) => {
        this.store.dispatch(new InitAction(forms));
          const db = database;
          db.forms.bulkAdd(forms);
        });
  }
}