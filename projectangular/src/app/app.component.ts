import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth/auth.service';
import { Store } from '@ngrx/store';
import { AppState } from './app.module';
import { Tag } from './model/tag.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Projecto Angular 6';
  tags : Tag[];
  constructor(public translate: TranslateService,private auth: AuthService, private store:Store<AppState>){
  	this.translate.getTranslation('en');
  	this.translate.setDefaultLang('es');
  	this.translate.use('es');
    store.subscribe((data) => this.tags = data.tracker.items);
  }

  getTags():Tag[]{
    this.store.subscribe((data) => this.tags = data.tracker.items);
    return this.tags;
  }

  isAuth():boolean{
  	return this.auth.isLoggedIn();
  }
}
