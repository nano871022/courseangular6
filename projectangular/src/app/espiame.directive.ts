import { Directive, ElementRef } from '@angular/core';
import { AppState } from './app.module';
import { Store } from '@ngrx/store';
import { Tags } from './model/objecto-state.model';
import { Tag } from './model/tag.model';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appEspiame]'
})
export class EspiameDirective {
  element : HTMLElement
  constructor(private elementRef: ElementRef, private store: Store<AppState>) {
  	this.element = elementRef.nativeElement;
  	fromEvent(this.element,"click").subscribe((evento) => this.track(evento));
   }


  track(evento: Event):void{
  	console.log("")
  	const tags = this.element.attributes.getNamedItem("data-tracker-tags").value.split(" ");
  	tags.forEach((tag)=>this.store.dispatch(new Tags(new Tag(tag))));
  }
}
