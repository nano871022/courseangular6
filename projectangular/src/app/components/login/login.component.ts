import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, ValidatorFn, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public formGroup : FormGroup;
  modal:boolean;

  constructor(formBuilder: FormBuilder, private auth: AuthService) { 
  	this.formGroup = formBuilder.group({
  		username: ['',Validators.compose([
  				this.validationUserName
  			])],
  		password: ['', Validators.compose([
  				this.validationPassword
  			])]
  	});
  }

  ngOnInit(): void {
  }

  login(username: string, password: string): boolean{
  	this.auth.login(username,password);
    this.modal = false;
  	return false;
  }

  validationPassword(control: FormControl):{[s: string]: boolean}{
  	const length = control.value.toString().trim().length;
	if( length < 7 || length > 100){
		return { valid: true};
	}
	return null;
  }

  validationUserName(control: FormControl):{[s: string]: boolean}{
	const length = control.value.toString().trim().length;
	if( length < 3 || length > 30){
		return { valid: true};
	}
	return null;
  }

  userName():string{
    return this.auth.getUser();
  }

  isLogin():boolean{
    return this.auth.isLoggedIn()
  }
  
  logout():boolean{
    return this.auth.logout();
  }
    showModal():boolean{
    this.modal = true;
    return false;
  }
  closeModal():boolean{
    this.modal = false;
    return false;
  }
  isShowModal():boolean{
    return this.modal;
  }
}
