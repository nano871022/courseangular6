import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Objecto } from '../../model/objecto.model'
import { FormGroup,FormBuilder,Validators,FormControl} from '@angular/forms';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app-formulario-object',
  templateUrl: './formulario-object.component.html',
  styleUrls: ['./formulario-object.component.scss']
})
export class FormularioObjectComponent implements OnInit {
	title : string;
  public formGroup : FormGroup;
  @Output() onEventEmitter:EventEmitter<Objecto>;

  constructor(formBuilder:FormBuilder) { 
	  this.title = "Formulario";
    this.onEventEmitter = new EventEmitter();
    this.formGroup = formBuilder.group({
       nombre:      ['',Validators.compose([
           Validators.required
           ,this.validatorName
         ])]
      ,descripcion: ['',Validators.compose([
          Validators.required
          ,this.validatorDescription
        ])]
    });
  }

  ngOnInit(): void {
  }

  agregar(nombre:string,descripcion:string):boolean{
    const obj = new Objecto(nombre,descripcion);
    this.onEventEmitter.emit(obj);
    console.log("Emitiendo objeto",obj);
  	return false;
  }

  validatorDescription(control:FormControl):{[campo:string]:boolean}{
    const size = control.value.trim().length
    console.log("descripcion validacion: ",size);
    if(size <= 3){
      return {valida:true};
    }
    return null;
  }

  validatorName(control:FormControl):{[campo:string]:boolean}{
    const size = control.value.trim().length
    console.log("nombre validacion: ",size);
    if(size <= 3){
      return {valida:true};
    }
    return null;
  }
}
