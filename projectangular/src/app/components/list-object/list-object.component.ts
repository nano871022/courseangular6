import { Component, OnInit, Output ,EventEmitter } from '@angular/core';
import { Objecto } from '../../model/objecto.model'
import { DataShared } from '../../model/data-shared.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { NuevoAction } from '../../model/objecto-state.model';
import { from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { ApiStore } from '../../model/api-data.model';

@Component({
  selector: 'app-list-object',
  templateUrl: './list-object.component.html',
  styleUrls: ['./list-object.component.scss']
})
export class ListObjectComponent implements OnInit {

  
  constructor(private store: ApiStore) {

  }

  ngOnInit(): void {
  }

  agregando(obj:Objecto):boolean{
  	console.log("recibiendo evento emitido por formulario");
    this.store.add(obj);
  	return false;
  }

  getAll():Objecto[]{
    return this.store.getAll();
  }

}
