import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Objecto } from '../../model/objecto.model';
import { ApiStore } from '../../model/api-data.model';
import { AppState } from '../../app.module';
import { LikeAction, ULikeAction } from '../../model/objecto-state.model';
import { style, trigger, state, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-objecto-detail',
  templateUrl: './objecto-detail.component.html',
  styleUrls: ['./objecto-detail.component.scss'],
  animations:[
    trigger("openMarker",[
        state("markerShow",style({backgroundColor:"white",overflow:"hidden"})),
        state("markerHidden",style({width:"0px",height:"0px",backgroundColor:"white",overflow:"hidden"})),
        transition("markerShow => markerHidden",[animate("3s")]),
        transition("markerHidden => markerShow",[animate("2s")])
      ])
  ]
})
export class ObjectoDetailComponent implements OnInit {
  @Input() objeto : Objecto;
  @HostBinding('attr.class') cssClass ="listado"
  map: any;
  popupShow: boolean = false;
  style = {
    sources: {
      world: {
        type: "geojson",
        data: "http://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json"
      }
    },
    version: 8,
    layers: [{
      "id": "countries",
      "type": "fill",
      "source": "world",
      "layout": {},
      "paint": {
        "fill-color": "#6F788A"
      }
    }]
  };

  constructor(private store: ApiStore) { }

  ngOnInit(): void {
  }

  elegido(obj:Objecto){
  	return false;
  }

  voteLike(){
    this.store.like(this.objeto);
  }

  voteULike(){
   this.store.unlike(this.objeto);
  }
  show(){
    this.popupShow = true;
  }
  hidden(){
    this.popupShow = false;
  }
  stateAnimation():string{
     return  this.popupShow?"markerShow":"markerHidden";
  }

}
