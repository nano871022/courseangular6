import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Store,ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListObjectComponent } from './components/list-object/list-object.component';
import { FormularioObjectComponent } from './components/formulario-object/formulario-object.component';
import { HomeComponent } from './components/home/home.component';
import { DataShared } from './model/data-shared.model';
import { ObjectoDetailComponent } from './components/objecto-detail/objecto-detail.component';
import { ObjectoEffects, ObjectoState, TrackState, reducerTags, reducerObjecto, initializeObjectoState, initializeTrackState } from './model/objecto-state.model';
import { AuthService } from './services/auth/auth.service';
import { LoginGuard } from './guards/login/login.guard';
import { ProtectedComponent } from './components/protected/protected.component';
import { LoginComponent } from './components/login/login.component';
import { DataBase } from './model/databaseconnect.model';
import { httpLoaderFactory, TranslationLoader } from './model/translation-loader.model';
import { APP_CONFIG, APP_CONFIG_API } from './model/api-config.model';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';  
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ApiStore ,InitializeApiStore } from './model/api-data.model';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';

const routes : Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: HomeComponent},
  { path: 'form', component: ListObjectComponent},
  { path: 'login', component: LoginComponent},
  { path: 'protected', component: ProtectedComponent, canActivate:[LoginGuard]}
];

//redux
export interface AppState{
  objectos: ObjectoState;
  tracker: TrackState;
};
const reducers: ActionReducerMap<AppState> = {
  objectos: reducerObjecto,
  tracker: reducerTags
};
let reducersInitialState = {
  objectos: initializeObjectoState(),
  tracker: initializeTrackState()
};

export function init_api_store(initApi: InitializeApiStore):() => Promise<any>{
  return ()=> initApi.initializeAppState();
}

@NgModule({
  declarations: [
    AppComponent,
    ListObjectComponent,
    FormularioObjectComponent,
    HomeComponent,
    ObjectoDetailComponent,
    ProtectedComponent,
    LoginComponent,
    EspiameDirective
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers,
      {
        initialState: reducersInitialState,
        runtimeChecks:{
          strictActionImmutability:false,
          strictStateImmutability:false
       }}),
    EffectsModule.forRoot([ObjectoEffects])
    ,TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (httpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [DataShared,AuthService,LoginGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_API},
    ApiStore,InitializeApiStore
    ,{provide: APP_INITIALIZER, useFactory: init_api_store, deps:[InitializeApiStore],multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
