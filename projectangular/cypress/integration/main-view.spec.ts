describe("Ventana principal",() => {
	it("tienen encabezaso correcto ",()=>{
		cy.visit("http://localhost:4200/home");
		cy.get("app-root header h4").contains("Project Angular 6");
	});

	it("have week 4",() => {
		cy.visit("http://localhost:4200/home");
		cy.get("li").contains("Semana 4 desarrollada");
	});
});

describe("Ventana form",() => {
	it("ingresar nombre y descripcion y click en agregar.",() => {
		cy.visit("http://localhost:4200/form");
		cy.get("#nombre").type("nombre").should("have.value","nombre");
		cy.get("#descripcion").type("descripcion").should("have.value","descripcion");
		cy.get(".btn",{timeout:200}).click();
	});
	it("verificar que se creo card",() => {
		cy.visit("http://localhost:4200/form");
		cy.get(".card-header").contains("nombre");
	});
});