var express = require("express");
var cors    = require("cors");
var app     = express();
var port    = 3001;
app.use(express.json());
app.use(cors());
app.listen(port,() => console.log("Server running over port "+port));
var translates = [{
        lang:"es",
        key:"user_name",
        value:"Nombre Usuario"
},{
        lang:"es",
        key:"password",
        value:"contraseña"
},{
        lang:"es",
        key:"input_user_name",
        value:"Ingrese el nombre de usuario"
},{
        lang:"es",
        key:"input_password",
        value:"Ingrese el password"
},{
        lang:"es",
        key:"value_invalid",
        value:"El valor ingresado es invalido"
},{
        lang:"en",
        key:"enter",
        value:"Enter"
},{
        lang:"en",
        key:"user_name",
        value:"User Name"
},{
        lang:"en",
        key:"password",
        value:"Password"
},{
        lang:"en",
        key:"input_user_name",
        value:"Input user name"
},{
        lang:"en",
        key:"input_password",
        value:"Input password"
},{
        lang:"en",
        key:"value_invalid",
        value:"The value is invalid"
},{
        lang:"es",
        key:"enter",
        value:"Ingresar"
},{
  lang:"en",
        key:"login",
        value:"Login"      
},{
  lang:"es",
        key:"login",
        value:"Iniciar Sesion"      
}];
app.get("/api/translate",(request,response,next) => response.json(translates.filter((translate) => translate.lang == request.query.lang)));
var forms = [];
app.post("/api/form",(request,response,next) => {
    var form = {
         name:request.query.name
        ,description:request.query.description
        ,like:0
        ,unlike:0
    };
    forms.push(form);
    response.json(form);
});
app.post("/api/like",(request,response,next) => {
    forms.filter((form) => form.name == request.query.name).forEach((form) => form.like++);
    response.json(forms.filter((form) => form.name == request.query.name));
});
app.post("/api/unlike",(request,response,next) => {
    forms.filter((form) => form.name == request.query.name).forEach((form) => form.unlike++);
    response.json(forms.filter((form) => form.name == request.query.name));
});
app.get("/api/forms",(request,response,next) => response.json(forms));