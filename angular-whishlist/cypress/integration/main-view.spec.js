describe("ventana principal",() => {
	it("tiene encabezado correcto y en español por defectyo",()=>{
	 cy.visit("http://localhost:4200");
	 cy.contains("angular-whishlist");
	 cy.get("h1 b").should("contain","hello");
	});
});

describe("agregar nuevo campo",() => {
	it("Genera un nav",()=>{
		cy.visit("http://localhost:4200");
		cy.get("#nombre").type("barcelona").should("have.value","barcelona");
		cy.get("#imagenUrl").type("http://goog.com").should("have.value","http://goog.com");
		cy.get(".btn",{timeout: 200}).click();
		cy.get(".card-content").contains("barcelona");
	});
});

describe("Ver informacion de tarjeta",() =>{
	it("Generar nav y ver informacion",() => {
		cy.visit("http://localhost:4200");
		cy.get("#nombre").type("bogota").should("have.value","bogota");
		cy.get("#imagenUrl").type("http://goog.com").should("have.value","http://goog.com");
		cy.get(".btn",{timeout: 200}).click();
		cy.get(".card-content",{timeout:500}).contains("bogota");
		cy.visit("http://localhost:4200/destino/1/");
		cy.get("p",{timeout:500}).contains("destino-detalle works!");
	});
});