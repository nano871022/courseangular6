import {DestinoViaje} from '../models/destino-viaje.model';
import {Subject, BehaviorSubject} from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState } from '../app.module';
import { Tags,ElegidoFavoritoAction, NuevosDestinoAction, RegisterTagAction } from '../models/destinos-viajes-state.model';
import { Injectable, Inject, forwardRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpRequest, HttpResponse, HttpHeaders } from '@angular/common/http';
import { APP_CONFIG, AppConfig, database } from '../app.module';

@Injectable()
export class DestinosApiClient{
	
	destinosViaje : DestinoViaje[];
	updates: string[];

	constructor(private store: Store<AppState> ,
		@Inject(forwardRef(() => APP_CONFIG))private config: AppConfig,
		private http: HttpClient ){
		this.updates = [];
		this.destinosViaje = [];
		this.store.select(state => state.destinos)
		.subscribe((data) => {
			this.destinosViaje = data.items;
		});
		this.store.select(state => state.destinos.favorito)
    		.subscribe(destino => {
      			if(destino != null){
        			this.updates.push('Se ha elegido a  '+destino.nombre);
      			}
    		});
    	this.store.select(state => state.tags).subscribe(tags =>{
    		tags.tags.forEach((tag) => {
    			var url = this.config.apiEndpoint+`/api/tag?name=${tag.name}&count=${tag.count}`
    			http.get(url).subscribe((response)=>console.log("save into api::",response));
    			console.log(`El tag: ${tag.name} tiene ${tag.count} interacciones `);
    		});
    	});
	}


	add(destinoViaje: DestinoViaje){
		const headers : HttpHeaders = new HttpHeaders({"X-API-TOKEN":"token-seguridad"});
		const request = new HttpRequest("POST", this.config.apiEndpoint+"/my",{nuevo:destinoViaje.nombre},{headers:headers});
		this.http.request(request).subscribe((response: HttpResponse<{}>) =>{
			if(response.status === 200){
				this.store.dispatch(new NuevosDestinoAction(destinoViaje));
				const myDb = database;
				myDb.destinos.add(destinoViaje);
				console.log("Todos los destinos de la db!");
				myDb.destinos.toArray().then(destinos => console.log(destinos));
			}	
		});
	}	

	getAll():DestinoViaje[]{
		return this.destinosViaje;
	}

	getById(id:string):DestinoViaje{
		return this.destinosViaje.filter( destino => destino.id.toString() == id)[0];
	}

	select(destinoViaje: DestinoViaje){
		this.destinosViaje.forEach(destino => destino.setSelected());
		destinoViaje.setSelected(true);
		this.store.dispatch(new ElegidoFavoritoAction(destinoViaje));
	}

	getUpdates():string[]{
    	return this.updates;
  	}

  	tag(tags:string[]){
  		console.log("Llego a la api");
  		tags.forEach((tag) => {
  			const newTag = new Tags();
  			newTag.name = tag;
  			this.store.dispatch(new RegisterTagAction(newTag));
  		});
  	}
	
}