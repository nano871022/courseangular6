import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { DestinoViaje } from './destino-viaje.model'
import { HttpClientModule } from '@angular/common/http';

export interface DestinoViajeState{
	items: DestinoViaje[];
	loading: boolean;
	favorito: DestinoViaje;
}

export class Tags{
	public name:string;
	public count:number;
	constructor(){
		this.count = 0;
	}
}

export interface TagsState{
	 tags: Tags[];
	 loading: boolean;
}

export const initializeDestinoViajeState  = function() {
	return {
		items: [],
		loading: false,
		favorito: null
	}
};

export const initializeTagsState = function(){
	return {
		tags: [],
		loading: false
	}
};	

export enum DestinosViajesActionTypes{
	NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
	ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito'
	,VOTE_UP = '[Destinos Viajes] Vore Up'
	,VOTE_DOWN = '[Destinos Viajes] Vote Down'
	,INIT_MY_DATA = '[Destinos Viaje] Init my Data'
}

export class InitMyDataAction implements Action{
	type = DestinosViajesActionTypes.INIT_MY_DATA;
	constructor(public destinos: string[]){}
}

export class NuevosDestinoAction implements Action{
	type = DestinosViajesActionTypes.NUEVO_DESTINO;
	constructor(public destinoViaje: DestinoViaje){

	}
}

export class VoteUpAction implements Action{
	type = DestinosViajesActionTypes.VOTE_UP;
	constructor(public destinoViaje: DestinoViaje){}
}

export class VoteDownAction implements Action{
	type = DestinosViajesActionTypes.VOTE_DOWN;
	constructor(public destinoViaje: DestinoViaje){}
}

export class ElegidoFavoritoAction implements Action{
	type = DestinosViajesActionTypes.ELEGIDO_FAVORITO;
	constructor(public destinoViaje: DestinoViaje){}
}

export type DestinoViajesAction = NuevosDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction; 
//REDURCERS
export function reducerDestinosViajes(state: DestinoViajeState,action: DestinoViajesAction){
	switch (action.type) {
		case DestinosViajesActionTypes.NUEVO_DESTINO:
			const vDestinoViaje = (action as NuevosDestinoAction).destinoViaje;
			const cantidad = state.items.length + 1;
			vDestinoViaje.setId(cantidad);
			return {
				...state,
				items:[...state.items,vDestinoViaje]
			}
		case DestinosViajesActionTypes.ELEGIDO_FAVORITO:
				state.items.forEach( destinoViaje => destinoViaje.setSelected());
				let fav: DestinoViaje = (action as ElegidoFavoritoAction).destinoViaje;
				fav.setSelected(true);
				return {
					...state,
					favorito: fav
				};
		case DestinosViajesActionTypes.VOTE_UP:
				const destinoViaje : DestinoViaje = (action as VoteUpAction).destinoViaje
				destinoViaje.voteUp();
				return {
					...state
				};
		case DestinosViajesActionTypes.VOTE_DOWN:
				const destinoViaje2 : DestinoViaje = (action as VoteDownAction).destinoViaje
				destinoViaje2.voteDown();
				return {
					...state
				};
		case DestinosViajesActionTypes.INIT_MY_DATA:{
				const destinos: string[] = (action as InitMyDataAction).destinos;
				var value: number = 1;
				return {
					...state,
					items: destinos.map((destinoViaje) => {
						const destino = new DestinoViaje(destinoViaje,"");
						destino.setId(value);
						value++;
						return destino;
					})
				};
		}
	}
	return state;
}
export enum TagsType{
	REGISTER = "[REGISTER] tags"
}
export class RegisterTagAction implements Action{
	type = TagsType.REGISTER;
	constructor(public tag: Tags){}
}
export type TagsAction = RegisterTagAction ;

export function reducerTags(state: TagsState, action: TagsAction){
	switch(action.type){
		case TagsType.REGISTER:
			const tag:Tags = (action as RegisterTagAction ).tag;
			tag.count++;
			const foundTags = state.tags.filter((value)=>value.name == tag.name)
			if(foundTags.length  > 0){
				foundTags.forEach((value) => value.count = value.count+1);
				return {...state};
			}
			return {...state,
			    tags:[...state.tags,tag]
			};
			
	}
	return state;
}

//EFFECTS
@Injectable({providedIn: 'root'})
export class DestinosViajesEffects{
	@Effect()
	nuevoAgregado: Observable<Action> = this.actions.pipe(
		ofType(DestinosViajesActionTypes.NUEVO_DESTINO),
		map((action:NuevosDestinoAction)=> new ElegidoFavoritoAction(action.destinoViaje))
		);
	constructor(private actions: Actions){}
}