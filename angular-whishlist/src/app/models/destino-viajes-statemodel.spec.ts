import{
	 reducerDestinosViajes
	,DestinoViajeState
	,initializeDestinoViajeState
	,InitMyDataAction
	,NuevosDestinoAction
	,ElegidoFavoritoAction
	,VoteUpAction
	,VoteDownAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes',() =>{
	it("should reduce init data",function(){
		//instance object
		const destinos = ["Destino 1","Destino 2"];
		const prevState : DestinoViajeState = initializeDestinoViajeState();
		const action: InitMyDataAction = new InitMyDataAction(destinos);
		//action 
		const newState: DestinoViajeState = reducerDestinosViajes(prevState,action);
		//asserts
		expect(newState.items.length).toEqual(2);
		expect(newState.items[0].nombre).toEqual(destinos[0]);
	});
	it('should reduce new item added',()=>{
		//instancia objecto
		const prevState = initializeDestinoViajeState();
		const destinoViaje = new DestinoViaje("barcelona","url");
		const action = new NuevosDestinoAction(destinoViaje);
		//action
		const result = reducerDestinosViajes(prevState,action);
		//asserts
		expect(result.items.length).toEqual(1);
		expect(result.items[0].nombre).toEqual(destinoViaje.nombre);
	});

	it("should reduce select favorite item",() => {
		const destinoViaje = new DestinoViaje("Destino1","https://irtl.com");
		const prevState = initializeDestinoViajeState();
		prevState.items.push(destinoViaje);
		const action = new ElegidoFavoritoAction(destinoViaje)
		//action
		const result = reducerDestinosViajes(prevState,action);
		//asserts
		expect(result.favorito.nombre).toEqual(destinoViaje.nombre)
		expect(result.favorito.isSelected()).toBeTrue();
		expect(result.items.length).toEqual(1);
	});

	it("should reduce vote up item",()=>{
		const destinoViaje = new DestinoViaje("Destino 2","http://asdasd.com");
		const prevState = initializeDestinoViajeState();
		prevState.items.push(new DestinoViaje("Destino 1","http://asdasd.com"));
		prevState.items.push(destinoViaje);
		const action = new VoteUpAction(destinoViaje);
		//action
		const result = reducerDestinosViajes(prevState,action);
		//asserts
		expect(result.items.length).toEqual(2);
		expect(result.items[1].nombre).toEqual(destinoViaje.nombre);
		expect(result.items[1].getVotes()).toEqual(1);
	});
	it("should reduce vote down item",()=>{
		const destinoViaje = new DestinoViaje("Destino 2","http://asdasd.com");
		const prevState = initializeDestinoViajeState();
		prevState.items.push(new DestinoViaje("Destino 1","http://asdasd.com"));
		prevState.items.push(destinoViaje);
		const action = new VoteDownAction(destinoViaje);
		//action
		const result = reducerDestinosViajes(prevState,action);
		//asserts
		expect(result.items.length).toEqual(2);
		expect(result.items[1].nombre).toEqual(destinoViaje.nombre);
		expect(result.items[1].getVotes()).toEqual(-1);
	});
});