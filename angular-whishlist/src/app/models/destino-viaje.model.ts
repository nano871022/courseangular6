export class DestinoViaje{
	selected : boolean = false;
	servicios : string[];
	id: number;
	votes: number = 0;
	constructor( public nombre:string,public imagenUrl:string){
		this.servicios = ['Desayuno','Pileta'];
	}
	isSelected():boolean{
		return this.selected;
	}
	setSelected(select:boolean=false){
		this.selected = select;
	}
	getServicios():string[]{
		return this.servicios;
	}

	setVotes(numero:number){
		this.votes = numero;
	}

	getVotes(){
		return this.votes;
	}

	setId(id:number){
		this.id = id;
	}

	getId():number{
		return this.id;
	}

	add(servicio:string){
		let position = this.servicios.length;
		this.servicios[position] = servicio;
	}

	voteUp(){
		this.votes++;
	}

	voteDown(){
		this.votes--;
	}
}