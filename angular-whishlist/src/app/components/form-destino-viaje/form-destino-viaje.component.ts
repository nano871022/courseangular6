import { Component, OnInit,Output,Input, EventEmitter, Inject,forwardRef } from '@angular/core';
import { DestinoViaje} from '../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl,ValidatorFn} from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged , switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { APP_CONFIG, AppConfig } from '../../app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
 @Output() onItemAdded: EventEmitter<DestinoViaje>;
 searchResult: string[];
 public formGroup1: FormGroup;
 minLength = 3;
 modal:boolean;

  constructor(formBuilder: FormBuilder,@Inject(forwardRef(() => APP_CONFIG))private config: AppConfig) { 
  	this.onItemAdded = new EventEmitter();
  	this.formGroup1 = formBuilder.group({
  		nombre: ['',Validators.compose([
        Validators.required,
        this.validationName,
        this.validationNameParametering(this.minLength)
        ])],
  		url: ['',Validators.compose([
        Validators.required,
        this.validationUrl
        ])]
  	});
    this.formGroup1.valueChanges.subscribe((form:any)=>{
      console.log('Cambio en el formulario: ',form);
    });
  }

  ngOnInit(): void {
    const elemName = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemName,'input')
    .pipe(
        map( (e:KeyboardEvent) => (e.target as HTMLInputElement).value),//genera observable de eventos de entrada
        filter(text => text.length > 2),
        debounceTime(200),//identifica el tiempo con el cual tocan las cadenas seguindas y genera un stop en el flujo hasta que pase el tiempo sin teclear
        distinctUntilChanged(),//freba el flujo hasta que llegue un valor diferente
        switchMap((ciudad: string) => ajax(this.config.apiEndpoint + "/ciudades?q=" + ciudad))//obtiene de un servicio web las ciudades segun lo puesto en el campo de texto
      ).subscribe(ajaxResponse => {
        const ciudades = ajaxResponse.response ;
        console.log(ciudades);
        this.searchResult = ciudades;
      })
  }

  guardar(nombre:string,url:string):boolean{
  	const destinoViaje = new DestinoViaje(nombre,url);
   	this.onItemAdded.emit(destinoViaje);  	
  	return false;
  }

  // funciones que ejecutan la logica de validacion

  validationName(control: FormControl):{[s: string]: boolean}{
    const length = control.value.toString().trim().length;
    if(length > 0 && length < 5){
      return {invalidName:true};
    }
    return null;
  }

  validationNameParametering(minLength: number):ValidatorFn{
    return (control: FormControl):{[s: string]: boolean} | null => {
      const length = control.value.toString().trim().length;
     if(length > 0 && length < this.minLength){
      return {minLengthName:true};
    } 
      return null;
    };
  }

  validationUrl(control: FormControl):{[s: string]: boolean} {
      const value = control.value.toString().trim();
      const validHttp = value.indexOf('http://')==0 || value.indexOf('https://') == 0;
      const validDot = value.indexOf(".") > 8;
      if(!validHttp || !validDot){
        return {urlValid:true};
      } 
      return null;
  }


}
