import { Component, OnInit, Inject, InjectionToken, Injectable } from '@angular/core';
import { DestinosApiClient } from '../../providers/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.scss','style.component.css']
  ,providers:[ DestinosApiClient ]
})
export class DestinoDetalleComponent implements OnInit {
  destino:DestinoViaje;
  map: any;
  style = {
    sources:{
      world:{
        type:'geojson',
        data: 'http://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version:8,
    layers:[{
      'id': 'countries',
      'type': 'fill',
      'source': 'world',
      'layout': {},
      'paint': {
        'fill-color': '#6F788A'
      }
    }]
  };


  constructor(private route:ActivatedRoute,public apiClient:DestinosApiClient) { }

  ngOnInit(): void {
  	let id = this.route.snapshot.paramMap.get('id');
  	console.log(id);
  	this.destino = this.apiClient.getById(id);
  	console.log(this.destino);
  }

}
