import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model'
import { DestinosApiClient} from '../../providers/destinos-api-client.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevosDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.scss']
  ,providers:[ DestinosApiClient ]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  
  constructor(private destinosApiClient: DestinosApiClient) { 
    this.onItemAdded = new EventEmitter();
  }

  ngOnInit(): void {
  }

  agregado(destinoViaje: DestinoViaje){
    this.destinosApiClient.add(destinoViaje);
    this.onItemAdded.emit(destinoViaje);
    return false;

}

getLista():DestinoViaje[]{
  return this.destinosApiClient.getAll();
}

  elegido(destinoViaje:DestinoViaje){
     this.destinosApiClient.select(destinoViaje)
  }

  getUpdates():string[]{
    return this.destinosApiClient.getUpdates();
  }
}
