import { Component, OnInit, Input, Output, HostBinding, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../../models/destino-viaje.model'
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from '../../models/destinos-viajes-state.model';
import { style, trigger, state, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.scss',"../../app.component2.css",]
  ,animations:[
    trigger('esFavorito',[
         state('estadoFavorito',style({backgroundColor:'#ccc',boxShadow:'0 10px 20px 0', position:"absolute",zIndex:"3"}))
        ,state('estadoNoFavorito',style({backgroundColor:"white"}))
        ,transition("estadoNoFavorito => estadoFavorito",[animate("3s")])
        ,transition("estadoFavorito => estadoNoFavorito",[animate("2s")])
      ])

  ]
})
export class DestinoViajeComponent implements OnInit {
	@Input() destino : DestinoViaje;
  @Input('idx') position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() clicked: EventEmitter<DestinoViaje>
	title : string
  constructor(private store: Store<AppState>) { 
  	this.title = "Formulario";
    this.clicked = new EventEmitter();
  }


  ir(){
    this.clicked.emit();
    return false;
  }

  ngOnInit(): void {
  }


  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  voteDown(){
     this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

  stateAnimation():string{
    return this.destino.isSelected()? "estadoFavorito" : "estadoNoFavorito";
  }
}
