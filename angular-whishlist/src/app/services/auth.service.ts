import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  LOCAL_STORAGE_USER_NAME: string = "USERNAME";
  constructor() { }

  login(username,password):boolean{
  	if(username === 'user' && password === 'password'){
  		localStorage.setItem(this.LOCAL_STORAGE_USER_NAME,username);
  		return true;
  	}
  	return false;
  }

  logout(): any{
  	localStorage.removeItem(this.LOCAL_STORAGE_USER_NAME);
  }

  getUser(): any{
  	return localStorage.getItem(this.LOCAL_STORAGE_USER_NAME);
  }

  isLoggedIn():boolean{
  	return this.getUser() != null;
  }
}
