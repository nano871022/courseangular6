import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient} from './providers/destinos-api-client.model';
import { reducerTags, initializeTagsState, TagsState, DestinoViajeState, reducerDestinosViajes, initializeDestinoViajeState,InitMyDataAction , DestinosViajesEffects} from './models/destinos-viajes-state.model';
import { Store,ActionReducerMap, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogeadoGuard } from './guards/usuario-logeado/usuario-logeado.guard';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import Dexie from 'dexie';
import { TranslateService, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive'

export const childrenRoutesVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent},
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent }
];

const routes : Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'home', component: ListaDestinosComponent},
  { path: 'destino/:id', component: DestinoDetalleComponent},
  { path: 'login', component: LoginComponent},
 { path: "protected", component: ProtectedComponent, canActivate:[UsuarioLogeadoGuard]},
  { path: 'vuelos', component: VuelosComponent, canActivate:[ UsuarioLogeadoGuard ], children:childrenRoutesVuelos}
];

//creando connecion a base de datos indexdb con framework dexie
export class Translation{
  constructor(public id: number, public lang:  string, public key: string, public value: string){}
}
@Injectable({providedIn:'root'})
export class MyDataBase extends Dexie{
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    super("MyDataBase");
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    })
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value'
    });
  }
}

export const database = new MyDataBase();


//servicio backed get|post /my
@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http:HttpClient){}
  async initializeDestinoViajeState():Promise<any>{
    const headers : HttpHeaders = new HttpHeaders({"X-API-TOKEN":"token-seguridad"});
    const request = new HttpRequest("GET", APP_CONFIG_VALUE.apiEndpoint+"/my",{headers: headers});
    const response : any = await this.http.request(request).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));    
  }
}

@Injectable()
class AppLoadIndexDB{
  constructor(private store: Store<AppState>){}
  async initializeDestinoViajeState():Promise<any>{
    database.destinos.toArray().then(destinos => 
      this.store.dispatch(new InitMyDataAction(destinos.map(destino => destino.nombre)))
    );
  }
}


export function init_app(appLoadService: AppLoadService): () => Promise<any>{
  return () => appLoadService.initializeDestinoViajeState();
}
export function init_app_db(appLoadIndexDB: AppLoadIndexDB):() => Promise<any>{
  console.log("ingreso a recuperacion de bd local");
  return () => appLoadIndexDB.initializeDestinoViajeState();
}
//servicio backend get /ciudades
export interface AppConfig{
  apiEndpoint: String;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3001'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// end servicio backenc
//redux
export interface AppState{
  destinos: DestinoViajeState;
  tags: TagsState;
};
const reducers: ActionReducerMap<AppState> = {
  destinos: reducerDestinosViajes
  ,tags: reducerTags
};
let reducersInitialState = {
  destinos: initializeDestinoViajeState()
  ,tags: initializeTagsState()
};
//fin reduc
// i18n begin
 class TranslationLoader implements TranslateLoader{
   constructor(private http: HttpClient){}
   getTranslation(lang: string):Observable<any>{
     const promise = database.translations.where("lang").equals(lang).toArray()
                     .then( results => {
                       if(results.length === 0){
                         return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint+"/api/translate?lang="+lang)
                                         .toPromise()
                                         .then(response => {
                                           database.translations.bulkAdd(response);
                                           return response;
                                         })                         
                       }
                       return results;
                     }).then((translations) => {
                       console.log("tradicciones cargadas");
                       console.log(translations);
                       return translations;
                     }).then((translations) => {
                       return translations.map((translation) => ({[translation.key]:translation.value}));
                     })
      return from(promise).pipe(flatMap((elems) => from(elems)));
   }
 }

 function HttpLoaderFactory(http: HttpClient){
   return new TranslationLoader(http);
 }

// I18n end
@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers,
      {
        initialState: reducersInitialState,
        runtimeChecks:{
          strictActionImmutability:false,
          strictStateImmutability:false
       }}),
    EffectsModule.forRoot([DestinosViajesEffects]),
    StoreDevtoolsModule.instrument(),
    ReservasModule
    ,TranslateModule.forRoot({
      loader:{
       provide: TranslateLoader
      ,useFactory: (HttpLoaderFactory)
      ,deps: [HttpClient]
    }})
    ,NgxMapboxGLModule
    ,BrowserAnimationsModule
  ],
  providers: [AuthService, UsuarioLogeadoGuard,AppLoadService,AppLoadIndexDB
  ,{provide: APP_CONFIG, useValue: APP_CONFIG_VALUE}
 // ,{provide: APP_INITIALIZER, useFactory: init_app, deps:[AppLoadService],multi:true}
  ,{provide: APP_INITIALIZER, useFactory: init_app_db, deps:[AppLoadIndexDB],multi:true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
