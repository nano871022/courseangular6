import { Component } from '@angular/core';
import {Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss','./app.component2.css']
})
export class AppComponent {
  time = new Observable(observer => {
  	setInterval(()=>observer.next(new Date().toString()),1000);
  });
  title = 'angular-whishlist';

  constructor(public translate: TranslateService){
  	console.log("***************** get transalation");
  	translate.getTranslation("en").subscribe(value => console.log("Value: "+JSON.stringify(value)));
  	translate.setDefaultLang("es");
  }
}
