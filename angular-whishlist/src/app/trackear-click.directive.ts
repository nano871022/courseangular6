import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';
import { DestinosApiClient } from './providers/destinos-api-client.model';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {
  private element: HTMLElement;

  constructor(private elRef: ElementRef,private api: DestinosApiClient) { 
  		this.element = elRef.nativeElement;
      console.log("llego a tracker");
  		fromEvent(this.element,'click').subscribe(evento => this.track(evento));
  }

  track(evento: Event):void{
  	const elemTags = this.element.attributes.getNamedItem("data-tracker-tags").value.split(' ');
  	console.log(`|||||| track evento: "${elemTags}"`);
    this.api.tag(elemTags);
  }
}
